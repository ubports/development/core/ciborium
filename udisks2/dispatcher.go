/*
 * Copyright 2015 Canonical Ltd.
 *
 * Authors:
 * Manuel de la Pena : manuel.delapena@cannical.com
 *
 * ciborium is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ciborium is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package udisks2

import (
	"log"
	_ "sort"
	"runtime"
	"strings"

	"github.com/godbus/dbus/v5"
)

const (
	jobPrefixPath    = "/org/freedesktop/UDisks2/jobs/"
	blockDevicesPath = "/org/freedesktop/UDisks2/block_devices/"
)

type Interfaces []string

type Event struct {
	Path       dbus.ObjectPath
	Props      InterfacesAndProperties
	Interfaces Interfaces
}

// isRemovalEvent returns if an event represents an InterfacesRemoved signal from the dbus ObjectManager
// dbus interface. An event is a removal event when it carries a set of the interfaces that have been lost
// in a dbus object path.
func (e *Event) isRemovalEvent() bool {
	return len(e.Interfaces) != 0
}

type dispatcher struct {
	conn           *dbus.Conn
	signals        chan *dbus.Signal
	Jobs           chan Event
	Additions      chan Event
	Removals       chan Event
}

// newDispatcher tries to return a dispatcher instance that is connected to the dbus signal that must be listened
// in order to interact with UDisk. If the connection with the signals could not be performed an error is returned.
func newDispatcher(conn *dbus.Conn) (*dispatcher, error) {
	log.Print("Creating new dispatcher.")

	d := &dispatcher{conn: conn}
	if err := conn.AddMatchSignal(
			dbus.WithMatchObjectPath(dbusObject),
			dbus.WithMatchInterface(dbusObjectManagerInterface),
			dbus.WithMatchSender(dbusName),
			dbus.WithMatchMember(dbusRemovedSignal),
		); err != nil {
		return nil, err
	}
	if err := conn.AddMatchSignal(
			dbus.WithMatchObjectPath(dbusObject),
			dbus.WithMatchInterface(dbusObjectManagerInterface),
			dbus.WithMatchSender(dbusName),
			dbus.WithMatchMember(dbusAddedSignal),
		); err != nil {
		d.removeSignals()
		return nil, err
	}

	d.signals = make(chan *dbus.Signal)
	d.Jobs = make(chan Event)
	d.Additions = make(chan Event)
	d.Removals = make(chan Event)

	runtime.SetFinalizer(d, cleanDispatcherData)

	// create the go routines used to grab the events and dispatch them accordingly
	return d, nil
}

func (d *dispatcher) Init() {
	log.Print("Init the dispatcher.")
	go func() {
		for s := range d.signals {
			var event Event
			if s.Name == dbusObjectManagerInterface + "." + dbusAddedSignal {
				if err := dbus.Store(s.Body, &event.Path, &event.Props); err != nil {
					log.Print("Failed to decode addition event: %v", err)
					continue
				}
				log.Print("New addition event for path ", event.Path, event.Props)
				d.processAddition(event)
			} else if s.Name == dbusObjectManagerInterface + "." + dbusRemovedSignal {
				if err := dbus.Store(s.Body, &event.Path, &event.Interfaces); err != nil {
					log.Print("Failed to decode removal event: %v", err)
					continue
				}
				log.Print("Removal event is ", event.Path, " Interfaces: ", event.Interfaces)
				d.processRemoval(event)
			}
		}
	}()
	d.conn.Signal(d.signals)
}

func (d *dispatcher) removeSignals() {
	d.conn.RemoveMatchSignal(
		dbus.WithMatchObjectPath(dbusObject),
		dbus.WithMatchInterface(dbusObjectManagerInterface),
		dbus.WithMatchSender(dbusName),
		dbus.WithMatchMember(dbusAddedSignal),
	)
	d.conn.RemoveMatchSignal(
		dbus.WithMatchObjectPath(dbusObject),
		dbus.WithMatchInterface(dbusObjectManagerInterface),
		dbus.WithMatchSender(dbusName),
		dbus.WithMatchMember(dbusRemovedSignal),
	)
}

func (d *dispatcher) free() {
	log.Print("Cleaning dispatcher resources.")
	// cancel all watches so that goroutines are done and close the
	// channels
	d.removeSignals()
	close(d.signals)
	close(d.Jobs)
	close(d.Additions)
	close(d.Removals)
}

func (d *dispatcher) processAddition(event Event) {
	log.Print("Processing an add event from path ", event.Path)
	// according to the object path we know if the even was a job one or not
	if strings.HasPrefix(string(event.Path), jobPrefixPath) {
		log.Print("Sending a new job event.")
		select {
		case d.Jobs <- event:
			log.Print("Sent event ", event.Path)
		}
	} else {
		log.Print("Sending a new general add event.")
		select {
		case d.Additions <- event:
			log.Print("Sent event ", event.Path)
		}
	}
}

func (d *dispatcher) processRemoval(event Event) {
	log.Print("Processing a remove event from path ", event.Path)
	// according to the object path we know if the even was a job one or not
	if strings.HasPrefix(string(event.Path), jobPrefixPath) {
		log.Print("Sending a new remove job event.")
		select {
		case d.Jobs <- event:
			log.Println("Sent event", event.Path)
		}
	} else {
		log.Print("Sending a new general remove event.")
		select {
		case d.Removals <- event:
			log.Println("Sent event", event.Path)
		}
	}
}

func cleanDispatcherData(d *dispatcher) {
	d.free()
}
