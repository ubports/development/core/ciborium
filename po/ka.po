# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the ciborium package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ciborium\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 17:38+0100\n"
"PO-Revision-Date: 2023-04-11 20:27+0000\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <https://hosted.weblate.org/projects/lomiri/ciborium/"
"ka/>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.17-dev\n"

#. TRANSLATORS: This is the summary of a notification bubble with a short message of
#. success when addding a storage device.
#: cmd/ciborium/main.go:123
msgid "Storage device detected"
msgstr "აღმოჩენილია საცავის მოწყობილობა"

#. TRANSLATORS: This is the body of a notification bubble with a short message about content
#. being scanned when addding a storage device.
#: cmd/ciborium/main.go:126
msgid "This device will be scanned for new content"
msgstr "ეს მოწყობილობა დათვალიერებული იქნება ახალი შემცველობისთვის"

#. TRANSLATORS: This is the summary of a notification bubble with a short message of
#. failure when adding a storage device.
#: cmd/ciborium/main.go:132
msgid "Failed to add storage device"
msgstr "საცავის მოწყობილობის დამატების შეცდომა"

#. TRANSLATORS: This is the body of a notification bubble with a short message with hints
#. with regards to the failure when adding a storage device.
#: cmd/ciborium/main.go:135
msgid "Make sure the storage device is correctly formated"
msgstr "დარწმუნდით, რომ საცავის მოწყობილობა სწორადაა დაფორმატებული"

#. TRANSLATORS: This is the summary of a notification bubble with a short message of
#. a storage device being removed
#: cmd/ciborium/main.go:141
msgid "Storage device has been removed"
msgstr "საცავის მოწყობილობა მოხსნილია"

#. TRANSLATORS: This is the body of a notification bubble with a short message about content
#. from the removed device no longer being available
#: cmd/ciborium/main.go:144
msgid ""
"Content previously available on this device will no longer be accessible"
msgstr ""
"შემცველობა, რომელიც ადრე ხელმისაწვდომი იყო ამ მოწყობილობაზე, ხელმისაწვდომი "
"აღარაა"

#. TRANSLATORS: This is the summary of a notification bubble with a short message warning on
#. low space
#: cmd/ciborium/main.go:329
msgid "Low on disk space"
msgstr "თქვენს დისკზე ცოტა ადგილიღაა დარჩენილი"

#. TRANSLATORS: This is the body of a notification bubble with a short message about content
#. reamining available space, %d is the remaining percentage of space available on internal
#. storage
#: cmd/ciborium/main.go:333
#, c-format
msgid "Only %d%% is available on the internal storage device"
msgstr "შიდა საცავ მოწყობილობაზე მხოლოდ %d%%-ღაა დარჩენილი"

#. TRANSLATORS: This is the body of a notification bubble with a short message about content
#. reamining available space, %d is the remaining percentage of space available on a given
#. external storage device
#: cmd/ciborium/main.go:337
#, c-format
msgid "Only %d%% is available on the external storage device"
msgstr "გარე საცავ მოწყობილობაზე მხოლოდ %d%%-ღაა დარჩენილი"

#: share/ciborium/qml/main.qml:21 share/applications/ciborium.desktop.tr.h:1
msgid "External Storage"
msgstr "გარე საცავი"

#: share/ciborium/qml/components/FormatDialog.qml:11
msgid "Continue with format"
msgstr "დაფორმატების გაგრძელება"

#: share/ciborium/qml/components/FormatDialog.qml:36
#: share/ciborium/qml/components/SafeRemoval.qml:36
msgid "Cancel"
msgstr "გაუქმება"

#: share/ciborium/qml/components/FormatDialog.qml:57
#: share/ciborium/qml/components/DriveDelegate.qml:54
msgid "Format"
msgstr "ფორმატი"

#: share/ciborium/qml/components/FormatDialog.qml:58
#: share/ciborium/qml/components/FormatDialog.qml:68
msgid "This action will wipe the content from the device"
msgstr "ეს ქმედება მოწყობილობის შემცველობად მთლიანად წაშლის"

#: share/ciborium/qml/components/FormatDialog.qml:67
msgid "Formatting"
msgstr "ფორმატირება"

#: share/ciborium/qml/components/FormatDialog.qml:89
msgid "Format Complete"
msgstr "დაფორმატება დასრულდა"

#: share/ciborium/qml/components/FormatDialog.qml:99
#: share/ciborium/qml/components/FormatDialog.qml:123
#: share/ciborium/qml/components/SafeRemoval.qml:82
#: share/ciborium/qml/components/SafeRemoval.qml:118
msgid "Ok"
msgstr "დიახ"

#: share/ciborium/qml/components/FormatDialog.qml:113
msgid "Format Error"
msgstr "ფორმატის შეცდომა"

#: share/ciborium/qml/components/FormatDialog.qml:114
msgid "There was an error when formatting the device"
msgstr "შეცდომა მოწყობილობის ფორმატირებისას"

#: share/ciborium/qml/components/SafeRemoval.qml:11
msgid "Continue"
msgstr "გაგრძელება"

#: share/ciborium/qml/components/SafeRemoval.qml:56
msgid "Confirm remove"
msgstr "წაშლის დადასტურება"

#: share/ciborium/qml/components/SafeRemoval.qml:57
msgid "Files on the device can't be accessed after removing"
msgstr "წაშლის შემდეგ მოწყობილობაზე ფაილებზე წვდომა აღარ გექნებათ"

#: share/ciborium/qml/components/SafeRemoval.qml:66
msgid "Unmounting"
msgstr "მიმაგრების მოხსნა"

#: share/ciborium/qml/components/SafeRemoval.qml:91
msgid "Safe to remove"
msgstr "მოსახსნელად უსაფრთხოა"

#: share/ciborium/qml/components/SafeRemoval.qml:92
msgid "You can now safely remove the device"
msgstr "ახლა შესაძლებელია მოწყობილობის უსაფრთხოდ გამოერთება"

#: share/ciborium/qml/components/SafeRemoval.qml:111
msgid "Unmount Error"
msgstr "მოხსნის შეცდომა"

#: share/ciborium/qml/components/SafeRemoval.qml:112
msgid "The device could not be unmounted because it is busy"
msgstr "მოწყობილობის მიმაგრების შეცდომა. ის დაკავებულია"

#: share/ciborium/qml/components/DriveDelegate.qml:59
msgid "Safely Remove"
msgstr "უსაფრთხოდ მოცილება"
